package com.paradm.common.constant;

public class GlobalConstant {

	public final static String DATE_FORMAT = "dd-MM-yyyy";
	public final static String DATETIME_FORMAT = "dd-MM-yyyy HH:mm";
	public final static String ENCODING = "UTF-8";
	public final static String LOGIN_SUCCESS = "S";
	public final static String LOCALE = "en_US";

	public static final String LANG_EN = "en";
	public static final String LANG_ZH = "zh";

	public final static String FILE_TYPE_IMAGE = "P";
	public final static String FILE_TYPE_VIDEO = "V";

	public final static String ROLE_ADMIN = "A";
	public final static String ROLE_USER = "U";

	public final static String STATUS_ACTIVE = "A";
	public final static String STATUS_INACTIVE = "I";

	public final static String CRYPT_MODE_ENCRYPT = "encrypt";
	public final static String CRYPT_MODE_DECRYPT = "decrypt";
	public final static String CRYPT_CRYPTO_SALT = "EncryptString2018";
	public final static String CRYPT_STR_SEPARATOR = ";";
	
}
