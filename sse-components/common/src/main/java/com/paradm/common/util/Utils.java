package com.paradm.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;

public class Utils {

	/*Empty Checking*/
	public static boolean isEmpty(String str) {
		return(str == null || str.length() == 0 || str.trim().equals("") );
	}

	public static boolean isEmpty(StringBuffer stringBuffer) {
		return(stringBuffer == null || stringBuffer.length() == 0 || stringBuffer.toString().trim().equals("") );
	}

	public static boolean isEmpty(Object[] array) {
		return(array == null || array.length == 0);
	}

	public static boolean isEmpty(String[] array) {
		return(array == null || array.length == 0);
	}

	public static boolean isEmpty(Integer val) {
		return(val == null || val == 0);
	}

	public static boolean isEmpty(Object val) {
		return(val == null || val == "" || val == " ");
	}

	public static boolean isEmpty(List<?> list) {
		return(list == null || list.size() == 0);
	}

	public static boolean isEmpty(Vector<?> vector) {
		return(vector == null || vector.size() == 0);
	}

	public static boolean isEmpty(InputStream inputStream) throws IOException {
		return(inputStream == null || inputStream.available() == 0 ||  IOUtils.toByteArray(inputStream) == null);
	}

	public static boolean isTrue(String str) {
		return(str == "IN" || str == "Y" || str == "true" || str == "True" || Boolean.valueOf(str) );
	}

	public static boolean isFalse(String str) {
		return(str == "OUT" || str == "N" || str == "false" || str == "False" || Boolean.valueOf(str) );
	}

	public static boolean isActive(String str) {
		return("A".equals(str) || str == "true" || str == "True" || Boolean.valueOf(str) );
	}

	public static boolean isExpiry(Date startDate, Date endDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(endDate);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		endDate = calendar.getTime();
		return(startDate.after(endDate));
	}

	public static Timestamp getCurrentTimestamp() {
		Calendar tmp = Calendar.getInstance();
		tmp.clear(Calendar.MILLISECOND);
		return(new Timestamp(tmp.getTime().getTime()));
	}

	public static String getStringCurrentDateTime() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");		
		return dateFormat.format(calendar.getTime());
	}

	public static String getStringCurrentDate() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");		
		return dateFormat.format(calendar.getTime());
	}

	public static String getRandomCode() {
		Random rand = new Random();
		int num = rand.nextInt(9000000) + 1000000;
		return Integer.toString(num);
	}

	public static int generatingRandomNumberInRange(int min, int max) {
		if (min >= max) throw new IllegalArgumentException("max must be greater than min");
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public static String generatingRandomString() {
		int length = 10;
		boolean useLetters = true;
		boolean useNumbers = false;
		String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
		System.out.println(generatedString);
		return generatedString;
	}

	public static String checkYN(String str) {
		if(isTrue(str)) {
			return "Y";
		} else {
			return "N";
		}
	}

}