# SSE - Customize Example

Customize Example with Spring Boot

Front End (sse-services/business/application)
- com.paradm.config

Controller / IController (sse-services/business/controller)
- com.paradm.controller
- com.paradm.controller.impl

Model / DTO (sse-services/business/domain)
- com.paradm.model

Service / IService (sse-services/business/sercice)
- com.paradm.service
- com.paradm.service.impl

Entity / DAO / IDAO / Repository / IRepository (sse-services/business/repository)
- com.paradm.entity
- com.paradm.repository
- com.paradm.repository.impl

Customize Services (sse-services/business/customize)
- com.paradm.customize.constant
- com.paradm.customize.controller
- com.paradm.customize.entity
- com.paradm.customize.model
- com.paradm.customize.repository
- com.paradm.customize.service

# Others

XML Settings
- sse-services/business/application/src/main/resources/application.properties
- sse-services/business/application/src/main/resources/logback-spring.xml
- sse-services/business/application/src/main/resources/sse-config.xml

Beans Settings
- com.paradm.config

Common Utils
- com.paradm.common.constant
- com.paradm.common.util
