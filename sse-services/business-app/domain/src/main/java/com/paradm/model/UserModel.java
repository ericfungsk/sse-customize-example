package com.paradm.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.paradm.common.constant.GlobalConstant;
import com.paradm.common.util.Utils;

import lombok.Data;

@Data
public class UserModel {

	private Long id;
	private String username;
	private String firstName;
	private String lastName;
	private String mobile;
	private String email;
	private String status;
	private Date createDate;
	private Date updateDate;
	private Set<TeamModel> teams;

	public UserModel() {
		this.status = GlobalConstant.STATUS_ACTIVE;
		this.createDate = Utils.getCurrentTimestamp();
		this.updateDate = Utils.getCurrentTimestamp();
		this.teams = new HashSet<TeamModel>();
	}

	public void setModel(String username, String firstName, String lastName){
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.status = GlobalConstant.STATUS_ACTIVE;
		this.createDate = Utils.getCurrentTimestamp();
		this.updateDate = Utils.getCurrentTimestamp();
		this.teams = new HashSet<TeamModel>();
	}

	public Set<String> getTeamsNameList() {
		HashSet<String> namesList = teams.stream()
				.map(TeamModel::getTeamName)
				.collect(Collectors.toCollection(HashSet::new));
		return namesList;
	}

}
