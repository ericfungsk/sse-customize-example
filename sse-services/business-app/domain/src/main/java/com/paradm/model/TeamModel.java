package com.paradm.model;

import java.util.Date;

import com.paradm.common.constant.GlobalConstant;
import com.paradm.common.util.Utils;

import lombok.Data;

@Data
public class TeamModel {

	private Long id;
	private String teamName;
	private String status;
	private Date createDate;
	private Date updateDate;

	public TeamModel() {
		this.status = GlobalConstant.STATUS_ACTIVE;
		this.createDate = Utils.getCurrentTimestamp();
		this.updateDate = Utils.getCurrentTimestamp();
	}

	public void setModel(String teamName){
		this.teamName = teamName;
		this.status = GlobalConstant.STATUS_ACTIVE;
		this.createDate = Utils.getCurrentTimestamp();
		this.updateDate = Utils.getCurrentTimestamp();
	}

}
