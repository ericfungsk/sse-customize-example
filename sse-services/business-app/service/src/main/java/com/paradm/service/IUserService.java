package com.paradm.service;

import java.util.List;

import com.paradm.model.UserModel;

public interface IUserService {

	public void save(UserModel userModel);
	public void savePassword(UserModel userModel, String password);
	public List<UserModel> findAll();
	public UserModel findById(Long id);
	public UserModel findByUsername(String username);
	public UserModel findByUsernameAndPassword(String username, String password);

}
