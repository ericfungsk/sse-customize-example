package com.paradm.service;

import java.util.List;

import com.paradm.model.TeamModel;

public interface ITeamService {

	public void save(TeamModel teamModel);
	public List<TeamModel> findAll();
	public TeamModel findById(Long id);
	public TeamModel findByTeamName(String teamName);

}
