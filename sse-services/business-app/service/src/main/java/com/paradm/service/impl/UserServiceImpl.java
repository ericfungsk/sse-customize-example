package com.paradm.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.model.UserModel;
import com.paradm.repository.IUserDao;
import com.paradm.service.IUserService;

public class UserServiceImpl implements IUserService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private IUserDao userDao;

	public void save(UserModel userModel) {
		userDao.save(userModel);
	}
	
	public void savePassword(UserModel userModel, String password) {
		userDao.savePassword(userModel, password);
	}
	
	public List<UserModel> findAll() {
		logger.debug("UserServiceImpl - findAll");
		List<UserModel> users = userDao.findAll();
		return users;
	}

	public UserModel findById(Long id) {
		logger.debug("UserServiceImpl - findById");
		return userDao.findById(id);
	}

	public UserModel findByUsername(String username) {
		logger.debug("UserServiceImpl - findByUsername");
		return userDao.findByUsername(username);
	}
	
	public UserModel findByUsernameAndPassword(String username, String password) {
		logger.debug("UserServiceImpl - findByUsernameAndPassword");
		return userDao.findByUsernameAndPassword(username, password);
	}
	

}
