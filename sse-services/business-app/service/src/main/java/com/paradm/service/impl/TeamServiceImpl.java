package com.paradm.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.model.TeamModel;
import com.paradm.repository.ITeamDao;
import com.paradm.service.ITeamService;

public class TeamServiceImpl implements ITeamService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired ITeamDao teamDao;

	public void save(TeamModel teamModel) {
		teamDao.save(teamModel);
	}

	public List<TeamModel> findAll() {
		logger.debug("TeamServiceImpl - findAll");
		List<TeamModel> teams = teamDao.findAll();
		return teams;
	}

	public TeamModel findById(Long id) {
		logger.debug("TeamServiceImpl - findById");
		return teamDao.findById(id);
	}

	public TeamModel findByTeamName(String teamName) {
		logger.debug("TeamServiceImpl - findByTeamName");
		return teamDao.findByTeamName(teamName);
	}

}
