package com.paradm.controller;

import java.util.Iterator;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public interface IUserRestController {

	//Get Media List Mapping
	@RequestMapping(value={""}, method = RequestMethod.GET)
	public Iterator<?> all();

	//Save User
	@RequestMapping(value={"add"}, method = RequestMethod.POST)
	public void save(@RequestParam(value="username", required=true) String username,
			@RequestParam(value="password", required=true) String password,
			@RequestParam(value="firstName", required=true) String firstName,
			@RequestParam(value="lastName", required=true) String lastName);
	
}
