package com.paradm.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;

import com.paradm.controller.IMainController;
import com.paradm.controller.constant.ControllerConstant;

public class MainControllerImpl implements IMainController {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	//Login Page Mapping
	public String loginView(Model model, String error, String logout) {
		logger.debug("##### Login Form");
		return ControllerConstant.VIEW_LOGIN;
	}

}
