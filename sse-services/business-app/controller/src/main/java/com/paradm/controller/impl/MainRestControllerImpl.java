package com.paradm.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.controller.IMainRestController;
import com.paradm.model.UserModel;
import com.paradm.service.IUserService;

public class MainRestControllerImpl implements IMainRestController {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private IUserService userService;

	//Mock Login Mapping
	public UserModel stubLogin(String username, String password) {
		logger.debug("##### Stub Login - " + username);
		UserModel user = userService.findByUsernameAndPassword(username, password);
		return user;
	}

}
