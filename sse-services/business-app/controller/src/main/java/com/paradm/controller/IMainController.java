package com.paradm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("")
public interface IMainController {

	//Login Page Mapping
	@RequestMapping(value={"", "login"}, method = RequestMethod.GET)
	public String loginView(Model model, @RequestParam(value="error", required=false) String error, @RequestParam(value="logout", required=false) String logout);

}
