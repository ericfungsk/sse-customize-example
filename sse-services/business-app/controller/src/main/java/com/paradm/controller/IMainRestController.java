package com.paradm.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paradm.model.UserModel;

@RestController
@RequestMapping("")
public interface IMainRestController {

	//Mock Login Mapping
	@RequestMapping(value={"mock/login"}, method = RequestMethod.POST)
	public UserModel stubLogin(@RequestParam(value="username", required=true) String username, @RequestParam(value="password", required=true) String password);

}
