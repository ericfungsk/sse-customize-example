package com.paradm.controller.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.controller.IUserRestController;
import com.paradm.model.UserModel;
import com.paradm.service.IUserService;

public class UserRestControllerImpl implements IUserRestController {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private IUserService userService;

	//Get User List Mapping
	public Iterator<UserModel> all(){
		logger.debug("List all Users");
		List<UserModel> users = userService.findAll();
		return users.iterator();
	}

	public void save(String username, String password, String firstName, String lastName) {
		logger.debug("Save User");

	}

}
