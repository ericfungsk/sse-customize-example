package com.paradm;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import com.paradm.model.TeamModel;
import com.paradm.model.UserModel;
import com.paradm.service.ITeamService;
import com.paradm.service.IUserService;

@SpringBootApplication
@ImportResource("classpath:sse-config.xml")
@ComponentScan(basePackages = {"com.paradm"})
public class WebApplication extends SpringBootServletInitializer {

	protected static final Logger logger = LoggerFactory.getLogger(WebApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebApplication.class);
	}

	public static void main(String[] args) throws Exception {
		logger.info("##### APPLICATION STARTING");
		SpringApplication sa = new SpringApplication(WebApplication.class);
		sa.setLogStartupInfo(false);
		sa.setBannerMode(Banner.Mode.OFF);
		sa.run(args);
		logger.info("##### APPLICATION STARTED");
	}

	@Autowired IUserService userService;
	@Autowired ITeamService teamService;

	@PostConstruct 
	public void init() throws Exception {
		logger.debug("##### APPLICATION Initialize");

		//Mock Test
		TeamModel team = new TeamModel();
		team.setModel("Team A");
		teamService.save(team);

		UserModel user = new UserModel();
		user.setModel("admin", "admin", "user");
		team = teamService.findByTeamName("Team A");
		user.getTeams().add(team);
		userService.savePassword(user, "123");

		logger.debug("##### APPLICATION Initialized");
	}

}
