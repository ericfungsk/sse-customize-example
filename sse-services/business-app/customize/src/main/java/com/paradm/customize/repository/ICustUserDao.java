package com.paradm.customize.repository;

import java.util.List;

import com.paradm.customize.model.CustUserModel;

public interface ICustUserDao {

	public void save(CustUserModel custUserModel);
	public List<CustUserModel> findAll();
	public CustUserModel findByUserCode(String userCode);
	public CustUserModel findByFirstName(String firstName);

}
