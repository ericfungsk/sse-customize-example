package com.paradm.customize.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.common.util.Utils;
import com.paradm.customize.entity.CustUser;
import com.paradm.customize.model.CustUserModel;
import com.paradm.customize.repository.ICustUserDao;
import com.paradm.customize.repository.ICustUserRepository;
import com.paradm.entity.User;
import com.paradm.model.UserModel;
import com.paradm.repository.IUserDao;
import com.paradm.repository.IUserRepository;

public class CustUserDaoImpl implements ICustUserDao {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private IUserDao userDao;
	@Autowired private IUserRepository userRepository;
	@Autowired private ICustUserRepository custUserRepository;
	@Autowired private ModelMapper modelMapper;

	public void save(CustUserModel custUserModel) {
		logger.debug("CustUserDaoImpl - save");
		User oldUser = userRepository.getOne(custUserModel.getId());
		if(!Utils.isEmpty(oldUser)) {
			User user = modelMapper.map(custUserModel, User.class); 
			user.setPassword(oldUser.getPassword());
			CustUser custUser = modelMapper.map(custUserModel, CustUser.class); 
			userRepository.save(user);
			custUserRepository.save(custUser);
		} else {
			logger.debug("CustUserDaoImpl - save : User not found");
		}
	}

	public List<CustUserModel> findAll() {
		logger.debug("CustUserDaoImpl - findAll");
		List<UserModel> userModels = userDao.findAll();
		List<CustUserModel> results = new ArrayList<CustUserModel>();
		for(UserModel userModel : userModels) {
			logger.debug("User [" + userModel.getFirstName() + "] found");
			Optional<CustUser> custUser = custUserRepository.findById(userModel.getId());
			if(custUser.isPresent()) {
				CustUserModel custUserModel = new CustUserModel();
				custUserModel.setModel(userModel, custUser.get().getUserCode());
				results.add(custUserModel);
			} else {
				logger.debug("Users not found");
			}
		}
		return results;
	}

	public CustUserModel findByUserCode(String userCode) {
		logger.debug("CustUserDaoImpl - findByUserCode");
		Optional<CustUser> custUser = custUserRepository.findByUserCode(userCode);
		if(custUser.isPresent()) {
			CustUserModel custUserModel = new CustUserModel();
			UserModel userModel = modelMapper.map(custUser.get().getUser(), UserModel.class); 
			custUserModel.setModel(userModel, custUser.get().getUserCode());
			return custUserModel;
		}
		return null;
	}

	public CustUserModel findByFirstName(String firstName) {
		logger.debug("CustUserDaoImpl - findByFirstName");
		UserModel user = userDao.findByFirstName(firstName);
		if(!Utils.isEmpty(user)) {
			logger.debug("User [" + user.getFirstName() + "] found");
			UserModel userModel = modelMapper.map(user, UserModel.class); 
			Optional<CustUser> custUser = custUserRepository.findById(userModel.getId());
			if(custUser.isPresent()) {
				CustUserModel custUserModel = new CustUserModel();
				custUserModel.setModel(userModel, custUser.get().getUserCode());
				return custUserModel;
			} else {
				logger.debug("User Code [" + firstName + "] not found");
			}
		} else {
			logger.debug("User [" + firstName + "] not found");
		}
		return null;
	}

}
