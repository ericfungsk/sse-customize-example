package com.paradm.customize.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.paradm.common.constant.GlobalConstant;
import com.paradm.common.util.Utils;
import com.paradm.model.TeamModel;
import com.paradm.model.UserModel;

import lombok.Data;

@Data
public class CustUserModel {

	private Long id;
	private String userCode;
	private String username;
	private String firstName;
	private String lastName;
	private String mobile;
	private String email;
	private String status;
	private Date createDate;
	private Date updateDate;
	private Set<TeamModel> teams;

	public CustUserModel() {
		this.status = GlobalConstant.STATUS_ACTIVE;
		this.createDate = Utils.getCurrentTimestamp();
		this.updateDate = Utils.getCurrentTimestamp();
		this.teams = new HashSet<TeamModel>();
	}

	public void setModel(UserModel userModel, String userCode) {
		this.id = userModel.getId();
		this.username = userModel.getUsername();
		this.firstName = userModel.getFirstName();
		this.lastName = userModel.getLastName();
		this.mobile = userModel.getMobile();
		this.email = userModel.getEmail();
		this.status = userModel.getStatus();
		this.createDate = userModel.getCreateDate();
		this.updateDate = userModel.getUpdateDate();
		this.teams = userModel.getTeams();
		this.userCode = userCode;
	}

}
