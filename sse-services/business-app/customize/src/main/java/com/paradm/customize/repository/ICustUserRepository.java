package com.paradm.customize.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paradm.customize.entity.CustUser;

public interface ICustUserRepository extends JpaRepository<CustUser, Long> {

	public Optional<CustUser> findByUserCode(String userCode);

}