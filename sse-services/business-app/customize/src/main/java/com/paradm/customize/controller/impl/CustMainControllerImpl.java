package com.paradm.customize.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;

import com.paradm.controller.IMainController;
import com.paradm.customize.constant.CustomizeConstant;

public class CustMainControllerImpl implements IMainController {

	//It contain/replace MainController function
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	//Login Page Mapping
	public String loginView(Model model, String error, String logout) {
		logger.debug("##### Cust Login Form");
		return CustomizeConstant.VIEW_LOGIN;
	}

}
