package com.paradm.customize.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.paradm.entity.User;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Entity
@Accessors(chain = true)
@Table(name = CustUser.TABLE_NAME)
public class CustUser implements Serializable {

	protected static final long serialVersionUID = 1L;
	protected static final String TABLE_NAME= "SSE_USER_CUST_DATA";

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id @Column(name="ID") 		private Long id;
	@Column(name="USER_CODE") 	private String userCode;

	@OneToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "id")
	private User user;

}
