package com.paradm.customize.controller.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.controller.IUserRestController;
import com.paradm.customize.controller.ICustUserRestController;
import com.paradm.customize.model.CustUserModel;
import com.paradm.customize.service.ICustUserService;

public class CustUserRestControllerImpl implements IUserRestController, ICustUserRestController {

	//It contain/replace UserRestController function and appended two more cust function
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private ICustUserService custUserService;

	//Get User List Mapping
	public Iterator<CustUserModel> all(){
		logger.debug("List all Users");
		List<CustUserModel> users = custUserService.findAll();
		return users.iterator();
	}

	public void save(String username, String password, String firstName, String lastName) {
		logger.debug("Save User");

	}

	//Cust Mapping
	public CustUserModel findByUserCode(String userCode) {
		logger.debug("Cust finding user by usercode");
		CustUserModel user = custUserService.findByUserCode(userCode);
		return user;
	}

	public CustUserModel findByFirstName(String firstName) {
		logger.debug("Cust finding user by first name");
		CustUserModel user = custUserService.findByFirstName(firstName);
		return user;
	}

}
