package com.paradm.customize.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.customize.model.CustUserModel;
import com.paradm.customize.repository.ICustUserDao;
import com.paradm.customize.service.ICustUserService;

public class CustUserServiceImpl implements ICustUserService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private ICustUserDao custUserDao;

	public void save(CustUserModel custUserModel) {
		custUserDao.save(custUserModel);
	}

	public List<CustUserModel> findAll() {
		logger.debug("CustUserServiceImpl - findAll");
		List<CustUserModel> users = custUserDao.findAll();
		return users;
	}

	public CustUserModel findByUserCode(String userCode) {
		logger.debug("CustUserServiceImpl - findByUserCode");
		return custUserDao.findByUserCode(userCode);
	}

	public CustUserModel findByFirstName(String firstName) {
		logger.debug("CustUserServiceImpl - findByFirstName");
		return custUserDao.findByFirstName(firstName);
	}

}
