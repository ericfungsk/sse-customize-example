package com.paradm.customize.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.paradm.customize.model.CustUserModel;

@RestController
@RequestMapping("user")
public interface ICustUserRestController {

	//Append two more Cust Mapping into UserRestController

	//Cust Mapping
	@RequestMapping(value={"userCode/{userCode}"}, method = RequestMethod.GET)
	public CustUserModel findByUserCode(@PathVariable(value="userCode", required=true) String userCode);

	@RequestMapping(value={"firstName/{firstName}"}, method = RequestMethod.GET)
	public CustUserModel findByFirstName(@PathVariable(value="firstName", required=true) String firstName);

}
