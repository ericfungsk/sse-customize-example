package com.paradm.customize.service;

import java.util.List;

import com.paradm.customize.model.CustUserModel;

public interface ICustUserService {

	public void save(CustUserModel custUserModel);
	public List<CustUserModel> findAll();
	public CustUserModel findByUserCode(String userCode);	
	public CustUserModel findByFirstName(String firstName);

}
