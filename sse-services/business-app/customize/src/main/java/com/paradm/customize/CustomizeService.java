package com.paradm.customize;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.customize.model.CustUserModel;
import com.paradm.customize.repository.ICustUserDao;
import com.paradm.model.UserModel;
import com.paradm.service.IUserService;

public class CustomizeService {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private ICustUserDao custUserDao;
	@Autowired private IUserService userService;

	@PostConstruct
	public void init() {
		logger.info("##### Customize Initialize - Module A ");

		//Mock Test
		UserModel userModel = userService.findByUsername("admin");
		CustUserModel custUserModel = new CustUserModel();
		custUserModel.setModel(userModel, "admin");
		custUserDao.save(custUserModel);

		logger.info("##### Customize Initialized - Module A ");
	}

}
