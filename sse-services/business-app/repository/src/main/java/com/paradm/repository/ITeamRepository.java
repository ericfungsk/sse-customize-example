package com.paradm.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paradm.entity.Team;

public interface ITeamRepository extends JpaRepository<Team, Long> {

	public Optional<Team> findByTeamName(String teamName);

}
