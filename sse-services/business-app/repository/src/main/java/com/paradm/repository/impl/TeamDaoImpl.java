package com.paradm.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.common.util.Utils;
import com.paradm.entity.Team;
import com.paradm.model.TeamModel;
import com.paradm.repository.ITeamDao;
import com.paradm.repository.ITeamRepository;

public class TeamDaoImpl implements ITeamDao {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private ITeamRepository teamRepository;
	@Autowired private ModelMapper modelMapper;

	public void save(TeamModel teamModel) {
		logger.debug("TeamDaoImpl - save");
//		Team oldTeam = teamRepository.getOne(teamModel.getId());
//		if(Utils.isEmpty(oldTeam)) {
			Team team = modelMapper.map(teamModel, Team.class); 
			teamRepository.save(team);
//		}
	}

	public List<TeamModel> findAll() {
		logger.debug("TeamDaoImpl - findAll");
		List<Team> teams = teamRepository.findAll();
		List<TeamModel> results = new ArrayList<TeamModel>();
		if(!Utils.isEmpty(teams)) {		
			results = teams.stream().map(this::convertToTeamModel).collect(Collectors.toList());
		} else {
			logger.debug("Teams not found");
		}
		return results;
	}

	public TeamModel findById(Long id) {
		logger.debug("TeamDaoImpl - findById");
		Optional<Team> team = teamRepository.findById(id);
		if(team.isPresent()) {
			TeamModel teamModel = modelMapper.map(team.get(), TeamModel.class); 
			return teamModel;
		}
		return null;
	}

	public TeamModel findByTeamName(String teamName){
		logger.debug("TeamDaoImpl - findByTeamName");
		Optional<Team> team = teamRepository.findByTeamName(teamName);
		if(team.isPresent()) {
			TeamModel teamModel = modelMapper.map(team.get(), TeamModel.class); 
			return teamModel;
		}
		return null;
	}

	//Utils
	public TeamModel convertToTeamModel(Team team) {
		TeamModel teamModel = modelMapper.map(team, TeamModel.class);
		return teamModel;
	}

}
