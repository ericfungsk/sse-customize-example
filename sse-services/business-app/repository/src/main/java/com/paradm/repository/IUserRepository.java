package com.paradm.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.paradm.entity.User;

public interface IUserRepository extends JpaRepository<User, Long> {

	public Optional<User> findByFirstName(String firstName);
	public Optional<User> findByUsername(String username);
	public Optional<User> findByUsernameAndPassword(String username, String password);

}
