package com.paradm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Entity
@Accessors(chain = true)
@Table(name = Team.TABLE_NAME)
public class Team implements Serializable {

	protected static final long serialVersionUID = 1L;
	protected static final String TABLE_NAME= "SSE_TEAM";

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id @Column(name = "ID", unique = true)	private Long id;
	@Column(name = "ROLE_NAME") 	private String teamName;
	@Column(name = "STATUS")		private String status;
	@Column(name = "CREATE_DATE")	private Date createDate;
	@Column(name = "UPDATE_DATE")	private Date updateDate;

	@ManyToMany(mappedBy="teams")
	private Set<User> users;

	@Override
	public int hashCode() {
		return Objects.hash(this.teamName);
	}
	
}