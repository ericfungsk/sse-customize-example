package com.paradm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Entity
@Accessors(chain = true)
@Table(name = User.TABLE_NAME)
public class User implements Serializable {

	protected static final long serialVersionUID = 1L;
	protected static final String TABLE_NAME= "SSE_USER";

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id @Column(name="ID", unique = true)	private Long id;
	@Column(name="USERNAME") 		private String username;
	@JsonIgnore
	@Column(name="PASSWORD") 		private String password;
	@Column(name="FIRST_NAME") 		private String firstName;
	@Column(name="LAST_NAME") 		private String lastName;
	@Column(name="MOBILE") 			private String mobile;
	@Column(name="EMAIL") 			private String email;
	@Column(name="STATUS")			private String status;
	@Column(name="CREATE_DATE")		private Date createDate;
	@Column(name="UPDATE_DATE")		private Date updateDate;

	@ManyToMany
	@JoinTable(name = "MTM_USER_TEAM", joinColumns = @JoinColumn(name="USER_ID", referencedColumnName="ID"), inverseJoinColumns = @JoinColumn(name="TEAM_ID", referencedColumnName="ID"))
	private Set<Team> teams;

}
