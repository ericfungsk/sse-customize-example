package com.paradm.repository;

import java.util.List;

import com.paradm.model.TeamModel;

public interface ITeamDao {

	public void save(TeamModel teamModel);
	public List<TeamModel> findAll();
	public TeamModel findById(Long id);
	public TeamModel findByTeamName(String teamName);

}
