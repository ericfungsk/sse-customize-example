package com.paradm.repository;

import java.util.List;

import com.paradm.model.UserModel;

public interface IUserDao {

	public void save(UserModel userModel);
	public void savePassword(UserModel userModel, String password);
	public List<UserModel> findAll();
	public UserModel findById(Long id);
	public UserModel findByFirstName(String firstName);
	public UserModel findByUsername(String username);
	public UserModel findByUsernameAndPassword(String username, String password);

}
