package com.paradm.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paradm.common.util.Utils;
import com.paradm.entity.User;
import com.paradm.model.UserModel;
import com.paradm.repository.IUserDao;
import com.paradm.repository.IUserRepository;

public class UserDaoImpl implements IUserDao {

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired private IUserRepository userRepository;
	@Autowired private ModelMapper modelMapper;

	public void save(UserModel userModel) {
		logger.debug("UserDaoImpl - save");
//		User oldUser = userRepository.getOne(userModel.getId());
//		if(Utils.isEmpty(oldUser)) {
			User user = modelMapper.map(userModel, User.class); 
			userRepository.save(user);
//		}
	}

	public void savePassword(UserModel userModel, String password) {
		logger.debug("UserDaoImpl - savePassword");
		User user = modelMapper.map(userModel, User.class); 
		user.setPassword(password);
		userRepository.save(user);
	}

	public List<UserModel> findAll() {
		logger.debug("UserDaoImpl - findAll");
		List<User> users = userRepository.findAll();
		List<UserModel> results = new ArrayList<UserModel>();
		if(!Utils.isEmpty(users)) {		
			results = users.stream().map(this::convertToUserModel).collect(Collectors.toList());
		} else {
			logger.debug("Users not found");
		}
		return results;
	}

	public UserModel findById(Long id) {
		logger.debug("UserDaoImpl - findById");
		Optional<User> user = userRepository.findById(id);
		if(user.isPresent()) {
			UserModel userModel = modelMapper.map(user.get(), UserModel.class); 
			return userModel;
		}
		return null;
	}

	public UserModel findByFirstName(String firstName) {
		logger.debug("UserDaoImpl - findByFirstName");
		Optional<User> user = userRepository.findByFirstName(firstName);
		if(user.isPresent()) {
			UserModel userModel = modelMapper.map(user.get(), UserModel.class); 
			return userModel;
		}
		return null;
	}


	public UserModel findByUsername(String username){
		logger.debug("UserDaoImpl - findByUsername");
		Optional<User> user = userRepository.findByUsername(username);
		if(user.isPresent()) {
			UserModel userModel = modelMapper.map(user.get(), UserModel.class); 
			return userModel;
		}
		return null;
	}

	public UserModel findByUsernameAndPassword(String username, String password) {
		logger.debug("UserDaoImpl - findByUsernameAndPassword");
		Optional<User> user = userRepository.findByUsernameAndPassword(username, password);
		if(user.isPresent()) {
			UserModel userModel = modelMapper.map(user.get(), UserModel.class); 
			return userModel;
		}
		return null;
	}

	//Utils
	public UserModel convertToUserModel(User user) {
		UserModel userModel = modelMapper.map(user, UserModel.class);
		return userModel;
	}

}
